package utils.reportPortal;

import com.google.common.io.BaseEncoding;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public  class TakeScreenshot {

    private static Logger logger = LoggerFactory.getLogger(TakeScreenshot.class);

    public static void now(Actor actor, String text) {
        byte[] screenshot = ((TakesScreenshot) BrowseTheWeb.as(actor).getDriver()).getScreenshotAs(OutputType.BYTES);

        logger.info("RP_MESSAGE#BASE64#{}#{}",
                BaseEncoding.base64().encode(screenshot),
                text);
    }
}
