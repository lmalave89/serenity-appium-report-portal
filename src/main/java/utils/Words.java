package utils;

import net.thucydides.core.util.SystemEnvironmentVariables;

public class Words {

    public static final String PLATFORM_NAME = "PLATFORM_NAME";
    public static final String AUTOMATION_NAME = "AUTOMATION_NAME";
    public static final String APP = "APP";
    public static final String DEVICE_NAME = "DEVICE_NAME";
    public static final String NO_RESET = "NO_RESET";
    public static final String URL = "URL";
    public static final String DIR = "user.dir";
    public static final String AUTOGRANT_PERMISSIONS = "autoGrantPermissions";
    public static final String TRUE = "true";
    public static final String ANDROID = "Android";
    public static final String IOS = "iOS";
    public static final String PLATFORM_SELECTED = SystemEnvironmentVariables.createEnvironmentVariables().getProperty(Words.PLATFORM_NAME);


}
