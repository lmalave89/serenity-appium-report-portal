package utils;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import net.thucydides.core.util.SystemEnvironmentVariables;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.net.MalformedURLException;
import java.net.URL;

public class WebDriverFactory {

    public static WebDriver getAppiumDriver() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, SystemEnvironmentVariables.createEnvironmentVariables().getProperty(Words.PLATFORM_NAME));
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, SystemEnvironmentVariables.createEnvironmentVariables().getProperty(Words.AUTOMATION_NAME));
        capabilities.setCapability(MobileCapabilityType.APP, System.getProperty(Words.DIR) + SystemEnvironmentVariables.createEnvironmentVariables().getProperty(Words.APP));
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, SystemEnvironmentVariables.createEnvironmentVariables().getProperty(Words.DEVICE_NAME));
        capabilities.setCapability(MobileCapabilityType.NO_RESET, SystemEnvironmentVariables.createEnvironmentVariables().getProperty(Words.NO_RESET));
        capabilities.setCapability(Words.AUTOGRANT_PERMISSIONS, Words.TRUE);
        return getDriver(capabilities);
    }

    private static WebDriver getDriver(DesiredCapabilities capabilities) throws MalformedURLException {
        if(Words.PLATFORM_SELECTED.equalsIgnoreCase(Words.ANDROID)){
            return new AndroidDriver(new URL(SystemEnvironmentVariables.createEnvironmentVariables().getProperty(Words.URL)), capabilities);
        }else{
            return new IOSDriver(new URL(SystemEnvironmentVariables.createEnvironmentVariables().getProperty(Words.URL)), capabilities);
        }
    }
}
