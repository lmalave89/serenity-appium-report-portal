package questions;


import exceptions.QuestionException;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.screenplay.waits.WaitUntil;
import utils.reportPortal.TakeScreenshot;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class GetTextQuestion implements Question<String> {

    private Target element;
    private String textValue;

    public GetTextQuestion(Target element){
        this.element = element;
    }

    @Override
    public String answeredBy(Actor actor) {
        try{
            WaitUntil.the(this.element, isVisible());
            this.textValue =  this.element.resolveFor(actor).getText();
            TakeScreenshot.now(actor, GetTextQuestion.class.getName());
        }catch (Exception e) {
            new QuestionException(QuestionException.error, e);
        }
        return this.textValue;
    }

    public static Question<String> value(Target element) {
        return new GetTextQuestion(element);
    }
}

