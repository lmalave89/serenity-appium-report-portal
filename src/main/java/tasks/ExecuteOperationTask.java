package tasks;

import exceptions.TaskException;
import interactions.ClickElement;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import ui.CalculatorPageUi;
import utils.Words;

public class ExecuteOperationTask implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        try{
            actor.attemptsTo(ClickElement.on(CalculatorPageUi.BTN_3.get(Words.PLATFORM_SELECTED)));
            actor.attemptsTo(ClickElement.on(CalculatorPageUi.BTN_4.get(Words.PLATFORM_SELECTED)));
            actor.attemptsTo(ClickElement.on(CalculatorPageUi.BTN_1.get(Words.PLATFORM_SELECTED)));
        }catch (Exception e){
            new TaskException(TaskException.error, e);
        }
    }


    public static ExecuteOperationTask on(){
        return Tasks.instrumented(ExecuteOperationTask.class);
    }
}
