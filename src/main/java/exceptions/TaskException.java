package exceptions;

import net.serenitybdd.core.exceptions.SerenityManagedException;

public class TaskException extends SerenityManagedException {

    public static final String error = "\n Failed executing the task";

    public TaskException(String message, Throwable testErrorException) {
        super(message + error, testErrorException);
    }
}
