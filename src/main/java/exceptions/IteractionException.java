package exceptions;

import net.serenitybdd.core.exceptions.SerenityManagedException;

public class IteractionException extends SerenityManagedException {

    public static final String error = "\n Failed executing the iteraction";

    public IteractionException(String message, Throwable testErrorException) {
        super(message + error, testErrorException);
    }
}
