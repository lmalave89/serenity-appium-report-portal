package exceptions;

import net.serenitybdd.core.exceptions.SerenityManagedException;

public class QuestionException extends SerenityManagedException {

    public static final String error = "\n Failed executing the question";

    public QuestionException(String message, Throwable testErrorException) {
        super(message + error, testErrorException);
    }
}

