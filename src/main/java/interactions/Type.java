package interactions;

import exceptions.IteractionException;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.screenplay.waits.WaitUntil;
import utils.reportPortal.TakeScreenshot;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class Type  implements Interaction {


    private Target element;
    private String text;

    public Type(Target element, String text){
        this.element = element;
        this.text = text;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        try{
            actor.attemptsTo(WaitUntil.the(this.element, isVisible()));
            actor.attemptsTo(Enter.theValue(this.text).into(this.element));
            TakeScreenshot.now(actor, ClickElement.class.getName());
        }catch (Exception e){
            new IteractionException(IteractionException.error, e);
        }
    }

    public static Type on(Target element, String text){
        return new Type(element, text);
    }
}
