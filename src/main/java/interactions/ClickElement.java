package interactions;

import exceptions.IteractionException;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.screenplay.waits.WaitUntil;
import utils.reportPortal.TakeScreenshot;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;


public class ClickElement  implements Interaction {

    private Target element;

    public ClickElement(Target element){
        this.element = element;
    }


    @Override
    public <T extends Actor> void performAs(T actor) {
        try{
            actor.attemptsTo(WaitUntil.the(this.element, isVisible()));
            actor.attemptsTo(Click.on(this.element));
            TakeScreenshot.now(actor, ClickElement.class.getName());
        }catch (Exception e){
            new IteractionException(IteractionException.error, e);
        }
    }

    public static ClickElement on(Target element){
        return new ClickElement(element);
    }

}
