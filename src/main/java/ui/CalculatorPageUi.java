package ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;
import utils.Words;
import java.util.HashMap;
import java.util.Map;

public class CalculatorPageUi extends PageObject {

    public static final Map<String, Target> BTN_1 = new HashMap<String, Target>() {
        {put(Words.ANDROID, Target.the("Button number one calculator").located(By.id("com.ba.universalconverter:id/buttons_row_1")));
            put(Words.IOS, Target.the("Button number one calculator").located(By.xpath("By define BTN_1")));
        }};

    public static final Map<String, Target> BTN_2 = new HashMap<String, Target>() {
        {put(Words.ANDROID, Target.the("Button number two calculator").located(By.id("com.ba.universalconverter:id/buttons_row_2")));
            put(Words.IOS, Target.the("Button number two calculator").located(By.xpath("By define BTN_2")));
        }};

    public static final Map<String, Target> BTN_3 = new HashMap<String, Target>() {
        {put(Words.ANDROID, Target.the("Button number three calculator").located(By.id("com.ba.universalconverter:id/buttons_row_3")));
            put(Words.IOS, Target.the("Button number three calculator").located(By.xpath("By define BTN_3")));
        }};

    public static final Map<String, Target> BTN_4 = new HashMap<String, Target>() {
        {put(Words.ANDROID, Target.the("Button number four calculator").located(By.id("com.ba.universalconverter:id/buttons_row_4")));
            put(Words.IOS, Target.the("Button number four calculator").located(By.xpath("By define BTN_4")));
        }};
}
