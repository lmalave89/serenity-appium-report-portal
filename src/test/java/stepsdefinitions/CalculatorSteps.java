package stepsdefinitions;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import tasks.ExecuteOperationTask;

import java.net.MalformedURLException;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class CalculatorSteps extends BaseTest{

    public CalculatorSteps() throws MalformedURLException {}

    @Given("^click in number ocho$")
    public void click_in_number_ocho(){
        theActorInTheSpotlight().attemptsTo(ExecuteOperationTask.on());
    }

    @When("^click in number nueve$")
    public void click_in_number_nueve(){
        System.out.println("Paso 2");
    }

    @Then("^validate result$")
    public void validate_result(){
        System.out.println("Paso 3");
    }
}
