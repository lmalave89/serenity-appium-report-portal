package stepsdefinitions;

import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import org.openqa.selenium.WebDriver;
import utils.WebDriverFactory;

import java.net.MalformedURLException;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;

public class BaseTest {

    private static WebDriver driver;
    private static String actor = "user";

    public BaseTest() throws MalformedURLException {
        if ((driver == null)) {
            Start();
        }
    }

    private static void Start() throws MalformedURLException {
        driver = WebDriverFactory.getAppiumDriver();
        OnStage.setTheStage(new OnlineCast());
        theActorCalled(actor).can(BrowseTheWeb.with(driver));
    }
}
