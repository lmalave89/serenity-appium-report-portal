package stepsdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import utils.reportPortal.TakeScreenshot;
import java.net.MalformedURLException;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class Hooks extends BaseTest{

    public Hooks() throws MalformedURLException {}

    @After
    public void close(Scenario scenario){
        if (scenario.isFailed()) {
            TakeScreenshot.now(theActorInTheSpotlight(), "Failed scenario test");
        }else{
            TakeScreenshot.now(theActorInTheSpotlight(), "Finished scenario test");
        }
    }
}
